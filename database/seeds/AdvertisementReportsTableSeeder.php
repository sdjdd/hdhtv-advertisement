<?php

use Illuminate\Database\Seeder;
use Carbon\CarbonPeriod;
use App\Models\Advertisement;
use App\Models\Report;

class AdvertisementReportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $advertisement = Advertisement::find(1);
        $positions = $advertisement->positions;
        $date_period = CarbonPeriod::create($advertisement->start_date, $advertisement->end_date);

        $data = [];
        foreach ($positions as $position) {
            foreach ($date_period as $date) {
                $reports = factory(Report::class, 20)->make();
                foreach ($reports as $report) {
                    $report->position_id = $position->id;
                    $report->date = $date;
                    $data[] = $report->toArray();
                }
            }
        }
        Report::insert($data);
    }
}
