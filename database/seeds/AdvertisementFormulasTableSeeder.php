<?php

use Illuminate\Database\Seeder;

class AdvertisementFormulasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Formula::class, 10)->create();
    }
}
