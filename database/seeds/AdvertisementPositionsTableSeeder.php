<?php

use Illuminate\Database\Seeder;
use App\Models\Advertisement;
use App\Models\Position;

class AdvertisementPositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ad_ids = Advertisement::select('id')->get()->pluck('id');
        $data = collect();
        foreach ($ad_ids as $id) {
            $positions = factory(Position::class, 5)->make();
            $positions->each(function ($position) use ($id) {
                $position->ad_id = $id;
                $position->created_at = $position->updated_at = now();
            });
            $data = $data->merge($positions);
        }
        Position::insert($data->toArray());

        $this->call(AdvertisementAccessesTableSeeder::class);
    }
}
