<?php

use Illuminate\Database\Seeder;
use App\Models\Position;
use App\Models\Access;

class AdvertisementAccessesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $position_ids = Position::select('id')->get()->pluck('id')->toArray();
        $data = [];
        foreach ($position_ids as $id) {
            $access = factory(Access::class)->make();
            $access->position_id = $id;
            $data[] = $access->toArray();
        }
        Access::insert($data);
    }
}
