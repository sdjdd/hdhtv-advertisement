<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(User $user)
    {
        $user->username = 'sdjdd';
        $user->password = bcrypt('secret');
        $user->is_admin = true;
        $user->save();
    }
}
