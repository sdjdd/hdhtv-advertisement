<?php

use Illuminate\Database\Seeder;

class AdvertisementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Advertisement::class, 50)->create();
        \DB::table('advertisements')->update(['user_id' => 1]);

        $this->call([
            AdvertisementPositionsTableSeeder::class,
            AdvertisementReportsTableSeeder::class,
        ]);
    }
}
