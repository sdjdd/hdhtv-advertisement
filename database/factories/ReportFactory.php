<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Report::class, function (Faker $faker) {
    $pv = $faker->numberBetween(300, 1000);
    $uv = $faker->numberBetween(300, $pv);
    return [
        'hotel_name' => $faker->words(3, true) . ' hotel',
        'pv' => $pv,
        'uv' => $uv,
        'published' => true,
    ];
});
