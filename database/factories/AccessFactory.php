<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Access::class, function (Faker $faker) {
    return [
        'time' => $faker->dateTime,
        'expected_count' => $faker->numberBetween(100, 500),
    ];
});
