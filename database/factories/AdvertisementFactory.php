<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Advertisement::class, function (Faker $faker) {
    $end_date = $faker->dateTimeThisMonth();
    $start_date = $faker->dateTimeThisMonth($end_date);
    return [
        'name' => $faker->company . '广告',
        'start_date' => $start_date,
        'end_date' => $end_date,
    ];
});
