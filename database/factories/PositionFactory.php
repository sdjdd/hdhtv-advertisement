<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Position::class, function (Faker $faker) {
    return [
        'name' => $faker->words(2, true),
        'url' => $faker->url,
    ];
});
