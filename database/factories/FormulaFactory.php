<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Formula::class, function (Faker $faker) {
    $content = [];
    for ($i = 0; $i < 24; $i++) {
        $min = $faker->numberBetween(0, 100);
        $max = $faker->numberBetween($min, 100);
        $content[] = [$min, $max];
    }
    return [
        'name' => $faker->words(3, true),
        'content' => json_encode($content),
    ];
});
