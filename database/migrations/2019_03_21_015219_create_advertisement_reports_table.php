<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisementReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisement_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('position_id')->unsigned();
            $table->date('date');
            $table->string('hotel_name');
            $table->string('platform_name')->default('电视');
            $table->integer('pv')->unsigned();
            $table->integer('uv')->unsigned();
            $table->boolean('published')->default(false);

            $table->index(['position_id', 'date']);

            $table->foreign('position_id')
                ->references('id')->on('advertisement_positions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisement_reports');
    }
}
