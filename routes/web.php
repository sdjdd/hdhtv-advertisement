<?php

// 首页
Route::get('/', 'StaticPageController@home')->name('home');

// 认证
Route::get('login', 'StaticPageController@login')->name('login');
Route::post('login', 'AuthController@login');
Route::get('logout', 'AuthController@logout')->name('logout');

// 广告监控
Route::prefix('monitor')->group(function () {
    Route::get('/', 'MonitorController@index')->name('monitor.index');
    Route::get('{advertisement}', 'MonitorController@detail')->name('monitor.detail');
});

// 后台管理
Route::prefix('admin')->group(function () {
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::prefix('advertisements')->group(function () {
        Route::get('/', 'AdminController@advertisementList')->name('admin.ad.list');
        Route::get('create', 'AdminController@advertisementCreate')->name('admin.ad.create');
        Route::post('create', 'AdminController@advertisementStore');
        Route::get('{advertisement}', 'AdminController@advertisementEdit')->name('admin.ad.edit');
        Route::post('{advertisement}', 'AdminController@advertisementUpdate');
        Route::delete('{advertisement}', 'AdminController@advertisementDelete');
    });
    Route::prefix('poisitions')->group(function () {
        Route::get('create', 'AdminController@positionCreate')->name('admin.position.create');
        Route::post('create', 'AdminController@positionStore');
        Route::get('{position}', 'AdminController@positionEdit')->name('admin.position.edit');
        Route::put('{position}', 'AdminController@positionUpdate');
    });
    Route::prefix('accesses')->group(function () {
        Route::get('create', 'AdminController@accessCreate')->name('admin.access.create');
        Route::post('create', 'AdminController@accessStore');
        Route::get('edit', 'AdminController@accessEdit')->name('admin.access.edit');
        Route::post('edit', 'AdminController@accessUpdate');
    });
    Route::prefix('formulas')->group(function () {
        Route::get('/', 'AdminController@formulaList')->name('admin.formula.list');
        Route::get('create', 'AdminController@formulaCreate')->name('admin.formula.create');
        Route::post('create', 'AdminController@formulaStore');
        Route::get('{formula}', 'AdminController@formulaEdit')->name('admin.formula.edit');
        Route::put('{formula}', 'AdminController@formulaStore');
        Route::delete('{formula}', 'AdminController@formulaDelete');
    });
    Route::prefix('users')->group(function () {
        Route::get('/', 'AdminController@userList')->name('admin.user.list');
        Route::get('create', 'AdminController@userCreate')->name('admin.user.create');
        Route::post('create', 'AdminController@userStore');
        Route::get('{user}', 'AdminController@userDetail')->name('admin.user.detail');
    });
    Route::prefix('actions')->group(function () {
        Route::get('/', 'AdminController@actionList')->name('admin.action.list');
        Route::get('{id}', 'AdminController@actionDetail')->name('admin.action.detail');
    });
});

Route::get('s/{hash}', 'StaticPageController@shareLink');

Route::prefix('dashboard')->group(function () {
    Route::get('lincoln/22393b76-bbac-4d9c-ba0a-03563541bccf', 'DashboardController@lincolnSanyaUsers');
    Route::get('lincoln/6247b65d-657f-45f6-a3ca-9c94076e9cd3', 'DashboardController@lincolnSanyaDriveUsers');
    Route::delete('lincoln/345ce653-48f7-4679-b7f1-721b587679f2', 'DashboardController@lincolnSanyaDelTransUser');
    Route::get('lincoln/{uuid}', 'DashboardController@lincolnSanyaTransUsers');
});

# 餐饮护理临时测试
Route::get('/tcloud/iotinterface/yaolaihudong/television/queryOwner', 'TestController@canyinhuli');

