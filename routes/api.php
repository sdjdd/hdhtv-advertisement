<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// 数据库同步工具
Route::prefix('v1/tool/sync')->group(function () {
    Route::get('{hotel_code}/last_id', 'ToolController@getLastOperationID');
    Route::post('{hotel_code}', 'ToolController@postOperationData');
});

// 广告工具
Route::prefix('tool/access')->middleware('tool')->group(function () {
    Route::get('tasks', 'AccessToolController@getTasks');
    Route::put('count', 'AccessToolController@putCount');
});

 Route::prefix('act')->group(function () {
    Route::prefix('lincoln')->group(function () {
        Route::post('/', 'Action\LincolnController@store');
        Route::post('record', 'Action\LincolnController@postUserData');
        Route::get('trans/available-time', 'Action\LincolnController@getAvailableTime');
        Route::post('trans', 'Action\LincolnController@postTransData');
        Route::post('drive', 'Action\LincolnController@postDriveData');
    });
 });