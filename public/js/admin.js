$('.ui.dropdown').dropdown()

function renderFormulaChart(data) {
    if (window.formulaChart === undefined) {
        let ctx = document.getElementById('formula_chart');
        let labels = []
        for (let i = 0; i < 24; i++) {
            labels.push(i < 10 ? `0${i}:00` : `${i}:00`)
        }
        let datasets = [{
            label: '最小值',
            backgroundColor: '#2185d0',
            borderColor: '#2185d0',
            fill: false,
        },{
            label: '最大值',
            backgroundColor: '#db2828',
            borderColor: '#db2828',
            fill: false,
        }]
        window.formulaChart = new Chart(ctx, {
            type: 'line',
            data: { labels, datasets },
        });
    }
    let datasets = window.formulaChart.data.datasets
    for (let i = 0; i < data.length; i++) {
        datasets[i].data = data[i]
    }
    formulaChart.update()
}

function renderAccessChart(data) {
    if (window.accessChart === undefined) {
        let ctx = document.getElementById('formula_chart');
        let labels = []
        for (let i = 0; i < 24; i++) {
            labels.push(i < 10 ? `0${i}:00` : `${i}:00`)
        }
        let datasets = [{
            label: '访问次数',
            backgroundColor: '#21ba45',
            borderColor: '#21ba45',
            fill: false,
        }]
        window.accessChart = new Chart(ctx, {
            type: 'line',
            data: { labels, datasets },
        });
    }
    window.accessChart.data.datasets[0].data = data
    accessChart.update()
}