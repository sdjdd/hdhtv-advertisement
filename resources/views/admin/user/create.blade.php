@extends('admin.layout')

@section('title', '添加用户')
@section('error', '添加失败')

@section('content')
<form class="ui form" method="POST">
  {{ csrf_field() }}
  <div class="field">
    <label>用户名</label>
    <input type="text" name="username" value="{{ old('username') }}" required autofocus>
  </div>
  <div class="field">
    <label>密码</label>
    <input type="password" name="password" required>
  </div>
  <div class="field">
    <label>确认密码</label>
    <input type="password" name="password_confirmation" required>
  </div>
  <div class="field">
    <label>备注</label>
    <textarea name="remark" rows="2" placeholder="可空">{{ old('remark') }}</textarea>
  </div>
  <button class="ui primary button" type="submit">保存</button>
</form>
@endsection