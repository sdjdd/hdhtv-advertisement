@extends('admin.layout')

@section('title', '用户详情')

@section('content')
<div class="ui medium header">基本信息</div>
<table class="ui celled table">
  <thead><tr>
    <th class="one wide">ID</th>
    <th class="six wide">用户名</th>
    <th class="six wide">备注</th>
    <th class="three wide">创建时间</th>
  </tr></thead>
  <tbody>
    <tr>
      <td>{{ $user->id }}</td>
      <td>{{ $user->username }}</td>
      <td>{{ $user->remark ?? '-' }}</td>
      <td>{{ $user->created_at }}</td>
    </tr>
  </tbody>
</table>

<div class="ui divider"></div>

<div class="ui medium header">广告列表</div>
<table class="ui celled table">
  <thead><tr>
    <th class="two wide">ID</th>
    <th class="six wide">广告名称</th>
    <th class="four wide">开始日期</th>
    <th class="four wide">结束日期</th>
  </tr></thead>
  <tbody>
    @if (count($user->advertisements) > 0)
      @foreach ($user->advertisements as $ad)
        <tr>
          <td>{{ $ad ->id }}</td>
          <td>
            <a href="{{ route('admin.ad.edit', $ad) }}">{{ $ad ->name }}</a>
          </td>
          <td>{{ parseDateString($ad ->start_date) }}</td>
          <td>{{ parseDateString($ad ->end_date) }}</td>
        </tr>
      @endforeach
    @else
      <tr><td class="nodata" colspan="4">暂无数据</td></tr>
    @endif
  </tbody>
</table>
@endsection