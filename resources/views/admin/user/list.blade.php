@extends('admin.layout')

@section('title', '用户列表')

@section('content')
  <div class="ui grid">
    <div class="twelve wide column">
      <a class="ui small green icon button" href="{{ route('admin.user.create') }}"  data-tooltip="添加用户" data-position="right center">
        <i class="add icon"></i>
      </a>
    </div>
    <div class="four wide column">
      <form class="ui small fluid icon input">
        <input name="search" type="text" placeholder="按用户名搜索..." value="{{ request('search') }}">
        <i class="search icon"></i>
      </form>
    </div>
  </div>
  @if (count($users) > 0)
    <table class="ui celled table">
      <thead><tr>
        <th class="one wide">ID</th>
        <th class="five wide">用户名</th>
        <th class="five wide">备注</th>
        <th class="three wide">创建时间</th>
        <th class="two wide">操作</th></tr>
      </thead>
      <tbody>
        @foreach ($users as $user)
          <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->username }}</td>
            <td>{{ $user->remark ?? '-' }}</td>
            <td>{{ $user->created_at }}</td>
            <td><a href="{{ route('admin.user.detail', $user) }}">查看</a></td>
          </tr>
        @endforeach
      </tbody>
      @if ($users->hasPages())
        <tfoot><tr><th colspan="5">
          {{ $users->links('_pagination', ['class' => 'mini right floated']) }}
        </th></tr></tfoot>
      @endif
    </table>
  @else
  <div class="ui message">
      <div class="header">列表为空</div>
      @if (request('search'))
        <p>没有与 "{{ request('search') }}" 匹配的数据</p>
      @else
        <p>暂无数据</p>
      @endif
    </div>
  @endif
@endsection