<form class="ui form" method="POST">
  {{ csrf_field() }}
  @if (isset($position))
    {{ method_field('PUT') }}
    <input type="hidden" name="id" value="{{ $position->id }}">
  @else
    <div class="field">
      <label>所属广告ID</label>
      <input type="number" name="ad_id" value="{{ request('ad_id') }}" required>
    </div>
  @endif
  <div class="field">
    <label>广告位名称</label>
    <input type="text" name="name" value="{{ old('name') ?? $position->name ?? '' }}" required autofocus>
  </div>
  <div class="field">
    <label>URL</label>
    <input type="text" name="url" value="{{ old('url') ?? $position->url ?? '' }}" placeholder="可空">
  </div>
  <div class="field">
    <label>备注</label>
    <textarea name="remark" rows="2" placeholder="可空">{{ old('remark') ?? $position->remark ?? '' }}</textarea>
  </div>
  <button class="ui primary button" type="submit">保存</button>
</form>