@extends('admin.layout')

@section('title', isset($position) ? '编辑广告位' : '创建广告位')
@section('error', isset($position) ? '更新失败' : '创建失败')

@section('breadcrumb')
  <div class="ui breadcrumb">
    <a class="section" href="{{ route('admin.ad.list') }}">广告列表</a>
    <i class="right angle icon divider"></i>
    <?php $advertisement = $advertisement ?? $position->advertisement ?>
    <a class="section" href="{{ route('admin.ad.edit', $advertisement) }}">
      {{ $advertisement->name }}
    </a>
    <i class="right angle icon divider"></i>
    <div class="active section">{{ isset($position) ? $position->name : '创建广告位' }}</div>
  </div>
@endsection

@section('content')
  <div class="ui medium header">基本信息</div>
  <form class="ui form" method="POST">
    {{ csrf_field() }}
    @if (isset($position))
      {{ method_field('PUT') }}
    @endif
    <div class="field">
      <label>广告位名称</label>
      <input type="text" name="name" value="{{ old('name') ?? $position->name ?? '' }}" required autofocus>
    </div>
    <div class="field">
      <label>URL</label>
      <input type="text" name="url" value="{{ old('url') ?? $position->url ?? '' }}" placeholder="可空">
    </div>
    <div class="field">
      <label>备注</label>
      <textarea name="remark" rows="2" placeholder="可空">{{ old('remark') ?? $position->remark ?? '' }}</textarea>
    </div>
    <button class="ui primary button" type="submit">保存</button>
  </form>

  @if (isset($position))
    <div class="ui divider"></div>
    <div class="ui medium header left floated" style="margin-top:.25rem;">
      访问记录
    </div>
    <a class="ui small green icon button right floated"
      href="{{ route('admin.access.create', ['position_id' => $position->id]) }}"
      data-tooltip="生成访问记录"
      data-position="left center">
      <i class="add icon"></i>
    </a>
    <table class="ui celled table">
      <thead><tr>
        <th>日期</th>
        <th>期望总量</th>
        <th>实际总量</th>
        <th>操作</th>
      </tr></thead>
      <tbody>
        @foreach ($accesses as $access)
          <tr>
            <td>{{ $access->date }}</td>
            <td>{{ $access->expected_count }}</td>
            <td>{{ $access->count }}</td>
            <td>
              <a href="{{ route('admin.access.edit', ['position_id' => $position->id, 'date' => $access->date]) }}">
                查看
              </a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  @endif
@endsection