@extends('admin.layout')

@section('title', '广告列表')

@section('content')
  <div class="ui grid">
    <div class="twelve wide column">
      <a class="ui small green icon button" href="{{ route('admin.ad.create') }}"  data-tooltip="创建广告" data-position="right center">
        <i class="add icon"></i>
      </a>
    </div>
    <div class="four wide column">
      <form class="ui small fluid icon input">
        <input name="search" type="text" placeholder="按名称搜索..." value="{{ request('search') }}">
        <i class="search icon"></i>
      </form>
    </div>
  </div>
  @if (count($advertisements) > 0)
    <table class="ui celled table">
      <thead><tr>
        <th>ID</th>
        <th>所属用户</th>
        <th>广告名称</th>
        <th>开始日期</th>
        <th>结束日期</th>
        <th>秒针</th>
        <th>操作</th></tr>
      </thead>
      <tbody>
        @foreach($advertisements as $ad)
          <tr>
            <td>{{ $ad->id }}</td>
            @if ($ad->user)
              <td><a href="{{ route('admin.user.detail', $ad->user)}}">{{ $ad->user->username }}</a></td>
            @else
              <td>-</td>
            @endif
            <td>{{ $ad->name }}</td>
            <td>{{ parseDateString($ad->start_date) }}</td>
            <td>{{ parseDateString($ad->end_date) }}</td>
            @if ($ad->access_enabled)
              <td class="positive">开启</td>
            @else
              <td class="negative">关闭</td>
            @endif
            <td><a href="{{ route('admin.ad.edit', $ad) }}">查看</a></td>
          </tr>
        @endforeach
      </tbody>
      @if ($advertisements->hasPages())
        <tfoot><tr><th colspan="8">
          {{ $advertisements->links('_pagination', ['class' => 'mini right floated']) }}
        </th></tr></tfoot>
      @endif
    </table>
  @else
  <div class="ui message">
      <div class="header">列表为空</div>
      @if (request('search'))
        <p>没有与 "{{ request('search') }}" 匹配的数据</p>
      @else
        <p>暂无数据</p>
      @endif
    </div>
  @endif
@endsection