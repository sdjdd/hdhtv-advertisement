@extends('admin.layout')

@section('title', isset($advertisement) ? '编辑广告' : '创建广告')
@section('error', isset($advertisement) ? '更新失败' : '创建失败')

@section('breadcrumb')
  <div class="ui breadcrumb">
    <a class="section" href="{{ route('admin.ad.list') }}">广告列表</a>
    <i class="right angle icon divider"></i>
    <div class="active section">{{ isset($advertisement) ? $advertisement->name : '创建广告' }}</div>
  </div>
@endsection

@section('content')
  <div class="ui medium header">基本信息</div>
  <form class="ui form" method="POST">
    {{ csrf_field() }}
    <div class="field">
      <label>广告名称</label>
      <input type="text" name="name" value="{{ old('name') ?? $advertisement->name ?? '' }}" required autofocus>
    </div>
    <div class="two fields">
      <div class="field">
        <label>所属用户ID</label>
        <input type="number" name="user_id" value="{{ old('user_id') ?? $advertisement->user_id ?? '' }}" placeholder="可空">
      </div>
      <div class="field">
        <label>活动ID</label>
        <input type="number" name="action_id" value="{{ old('action_id') ?? $advertisement->action_id ?? '' }}" placeholder="可空">
      </div>
    </div>
    <div class="two fields">
      <div class="field">
        <label>开始日期</label>
        <input type="date" name="start_date" value="{{ old('start_date') ?? $advertisement->start_date ?? '' }}" required>
      </div>
      <div class="field">
        <label>结束日期</label>
        <input type="date" name="end_date" value="{{ old('end_date') ?? $advertisement->end_date ?? '' }}" required>
      </div>
    </div>
    <div class="field">
      <label>备注</label>
      <textarea name="remark" rows="2" placeholder="可空">{{ old('remark') ?? $advertisement->remark ?? '' }}</textarea>
    </div>
    <div class="field">
      <div class="ui toggle checkbox">
        <input type="checkbox" id="access_enabled" name="access_enabled" @if(old('access_enabled') ?? $advertisement->access_enabled ?? false) checked @endif>
        <label for="access_enabled">开启广告监控工具</label>
      </div>
    </div>
    <button class="ui primary button" type="submit">保存</button>
    @if (isset($advertisement))
      <button class="negative ui right floated button" type="submit" form="delete_form">删除</button>
    @endif
  </form>

  @if (isset($advertisement))
    <form id="delete_form" method="POST" style="margin-top: 1rem;"
      onsubmit="return confirm('确定要删除该广告吗？')" >
      {{ method_field('DELETE') }}
      {{ csrf_field() }}
    </form>

    <div class="ui divider"></div>
    <div class="ui medium header left floated" style="margin-top:.25rem;">
      广告位列表
    </div>
    <a class="ui small green icon button right floated"
      href="{{ route('admin.position.create', ['ad_id' => $advertisement->id]) }}"
      data-tooltip="创建广告位" data-position="left center">
      <i class="add icon"></i>
    </a>

    <table class="ui celled table">
      <thead><tr>
        <th class="two wide">ID</th>
        <th class="six wide">名称</th>
        <th class="six wide">备注</th>
        <th class="two wide">操作</th>
      </tr></thead>
      <tbody>
        @if (count($advertisement->positions) > 0)
          @foreach ($advertisement->positions as $position)
            <tr>
              <td>{{ $position->id }}</td>
              <td>{{ $position->name }}</td>
              <td>{{ $position->remark ?? '-' }}</td>
              <td><a href="{{ route('admin.position.edit', $position) }}">查看</a></td>
            </tr>
          @endforeach
        @else
          <tr><td class="nodata" colspan="4">暂无数据</td></tr>
        @endif
      </tbody>
    </table>
  @endif
@endsection