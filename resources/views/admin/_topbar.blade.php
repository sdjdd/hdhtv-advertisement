<div class="ui top fixed menu">
  <div class="ui container">
    <a class="item" href="{{ route('admin.index') }}">HDHTV</a>
    <a class="item" href="{{ route('monitor.index') }}">广告监控</a>
    <div class="right menu">
      <div class="ui dropdown item" style="min-width:90px;">
        {{ Auth::user()->username }} <i class="dropdown icon"></i>
        <div class="menu">
          <a class="item" href="{{ route('logout') }}">注销登录</a>
        </div>
      </div>
    </div>
  </div>
</div>