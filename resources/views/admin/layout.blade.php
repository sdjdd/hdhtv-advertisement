<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <meta charset="UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>后台管理 | @yield('title', '首页')</title>
  <link rel="stylesheet" href="{{ asset('asset/semantic-ui-2.4.1/semantic.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
</head>
<body>
  @include('admin._topbar')
  <div class="ui container grid main-box">
      <div class="three wide column">
        @include('admin._sidebar')
      </div>
      <div class="thirteen wide column">
        @yield('breadcrumb')
        <div class="ui segment">
          @if (count($errors) > 0)
            <div class="ui error message">
              <div class="header">@yield('error', '错误')</div>
              <ul class="list">
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          @foreach (['success', 'info', 'warning', 'error'] as $msg)
            @if (session()->has($msg))
              <div class="ui {{ $msg }} message">
                <p>{{ session()->get($msg) }}</p>
              </div>
            @endif
          @endforeach

          <div class="ui large header">@yield('title')</div>
          @yield('content')
        </div>
      </div>
    </div>
<script src="{{ asset('asset/jquery-3.3.1/jquery.min.js') }}"></script>
<script src="{{ asset('asset/semantic-ui-2.4.1/semantic.min.js') }}"></script>
<script src="{{ asset('asset/chart.js-2.8.0/Chart.min.js') }}"></script>
<script src="{{ asset('js/admin.js') }}"></script>
@stack('scripts')
</body>
</html>