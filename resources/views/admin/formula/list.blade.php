@extends('admin.layout')

@section('title', '公式列表')

@section('content')
  <div class="ui grid">
    <div class="twelve wide column">
      <a class="ui small green icon button" href="{{ route('admin.formula.create') }}" data-tooltip="添加公式" data-position="right center">
        <i class="add icon"></i>
      </a>
    </div>
  </div>
  <table class="ui celled table">
    <thead><tr>
      <th class="one wide">ID</th>
      <th class="four wide">名称</th>
      <th>备注</th>
      <th class="three wide">创建时间</th>
      <th class="one wide">操作</th></tr>
    </thead>
    <tbody>
      @if (count($formulas) > 0)
        @foreach($formulas as $formula)
          <tr>
            <td>{{ $formula->id }}</td>
            <td>{{ $formula->name }}</td>
            <td>{{ $formula->remark ?: '-' }}</td>
            <td>{{ $formula->created_at }}</td>
            <td>
              <a href="{{ route('admin.formula.edit', $formula) }}">编辑</a>
            </td>
          </tr>
        @endforeach
      @else
        <tr><td class="nodata" colspan="5">暂无数据</td></tr>
      @endif
    </tbody>
    @if ($formulas->hasPages())
      <tfoot><tr><th colspan="8">
        {{ $formulas->links('_pagination', ['class' => 'mini right floated']) }}
      </th></tr></tfoot>
    @endif
  </table>
@endsection