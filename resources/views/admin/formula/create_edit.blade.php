@extends('admin.layout')

@section('title', isset($formula) ? '编辑公式' : '添加公式')
@section('error', isset($formula) ? '更新失败' : '添加失败')

@section('content')
  @if (isset($formula))
    <form id="delete_form" method="POST" onsubmit="return confirm('确定要删除该公式吗？')">
      {{ csrf_field() }}
      {{ method_field('DELETE') }}
    </form>
  @endif

  <form class="ui form" method="POST">
    {{ csrf_field() }}
    @if (isset($formula))
      {{ method_field('PUT') }}
    @endif
    <div class="field">
      <label>公式名称</label>
      <input type="text" name="name" value="{{ old('name') ?? $formula->name ?? '' }}" required autofocus>
    </div>
    <div class="field">
      <label>备注</label>
      <textarea name="remark" rows="2" placeholder="可空">{{ old('remark') ?? $formula->remark ?? '' }}</textarea>
    </div>

    @include('admin._formula_chart')
    
    <div style="margin: 10px 0;">
      <button class="ui green button" onclick="updateFormulaChart()" form="">预览</button>
      <button class="ui primary button" type="submit">保存</button>
      @if (isset($formula))
        <button class="ui negative right floated button" form="delete_form">删除</button>
      @endif
    </div>
    
    <table class="ui celled definition table" id="formula_table">
      <thead><tr>
        <th class="two wide"></th>
        <th>最小值</th>
        <th>最大值</th>
      </tr></thead>
      <tbody>
        @for ($i = 0; $i < 24; $i++)
          <tr>
            <td>{{ ($i < 10 ? "0{$i}" : $i) . ':00' }}</td>
            <td>
              <div class="ui mini fluid input">
                <input name="min[]" type="number" value="{{ old("min.{$i}") ?? $formula->content[$i][0] ?? 0 }}" required>
              </div>
            </td>
            <td>
              <div class="ui mini fluid input">
                <input name="max[]" type="number" value="{{ old("max.{$i}") ?? $formula->content[$i][1] ?? 0 }}" required>
              </div>
            </td>
          </tr>
        @endfor
      </tbody>
    </table>
  </form>
@endsection

@push('scripts')
<script>
  updateFormulaChart()
  function updateFormulaChart() {
    let min = [], max = []
    $('#formula_table input[name="min[]"]').each((i, e) => {
        min.push(Number($(e).val()))
    })
    $('#formula_table input[name="max[]"]').each((i, e) => {
        max.push(Number($(e).val()))
    })
    renderFormulaChart([min, max])
  }
</script>
@endpush