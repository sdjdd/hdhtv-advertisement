<div class="ui vertical menu side-bar">
  <a class="item {{ active_class(if_route_pattern('admin.ad*')) }}"
    href="{{ route('admin.ad.list') }}">
    广告
  </a>
  <a class="item {{ active_class(if_route_pattern('admin.formula*')) }}"
    href="{{ route('admin.formula.list') }}">
    公式
  </a>
  <a class="item {{ active_class(if_route_pattern('admin.user*')) }}"
    href="{{ route('admin.user.list') }}">
    用户
  </a>
  <a class="item {{ active_class(if_route_pattern('admin.action*')) }}"
    href="{{ route('admin.action.list') }}">
    活动
  </a>
</div>