<div class="ui breadcrumb">
  <a class="section" href="{{ route('admin.ad.list') }}">广告列表</a>
  <i class="right angle icon divider"></i>
  <a class="section" href="{{ route('admin.ad.edit', $position->advertisement) }}">
    {{ $position->advertisement->name }}
  </a>
  <i class="right angle icon divider"></i>
  <a class="section" href="{{ route('admin.position.edit', $position) }}">{{ $position->name }}</a>
  <i class="right angle icon divider"></i>
  <div class="active section">{{ isset($accesses) ? '编辑访问记录' : '生成访问记录' }}</div>
</div>