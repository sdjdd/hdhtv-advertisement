@extends('admin.layout')

@section('title', '编辑访问记录')
@section('error', "更新失败")

@section('breadcrumb')
  @include('admin.access._breadcrumb')
@endsection

@section('content')
  <div class="ui segment">
    <canvas id="formula_chart" style="width:100%;"></canvas>
  </div>

  <div class="ui header">
    {{ parseDateString(request('date')) }} - 共 <span id="access_sum">计算中...</span> 次
  </div>

  <form class="ui form" id="access_form" method="POST">
    {{ csrf_field() }}
    <div class="fields">
      @foreach ($accesses as $access)
        <div class="field">
          <label>{{ ($loop->index < 10 ? '0'.$loop->index : $loop->index).':00' }}</label>
          <input type="hidden" name="id[]" value="{{ $access->id }}">
          <input type="hidden" name="old[]" value="{{ $access->expected_count }}">
          <input type="number" name="data[]" value="{{ $access->expected_count }}">
        </div>
        @if ($loop->iteration % 8 === 0)
          </div><div class="fields">
        @endif
      @endforeach
    </div>
    <button class="ui primary button" type="submit">保存</button>
  </form>
@endsection

@push('scripts')
<script>
  updateFormulaChart()
  function updateFormulaChart() {
    let data = [], sum = 0
    $('#access_form input[name="data[]"]').each((i, e) => {
      let val = Number($(e).val())
      sum += val
      data.push(val)
    })
    renderAccessChart(data)
    $('#access_sum').text(sum)
  }
</script>
@endpush