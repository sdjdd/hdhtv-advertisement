@extends('admin.layout')

@section('title', '生成访问记录')
@section('error', "生成失败")

@section('breadcrumb')
  @include('admin.access._breadcrumb')
@endsection

@section('content')
  <form class="ui form" method="POST">
    {{ csrf_field() }}
    <div class="two fields">
      <div class="field">
        <label>日期</label>
        <select class="ui fluid dropdown" name="date">
          @foreach ($dates as $date)
            <option value="{{ $date }}">{{ $date }}</option>
          @endforeach
        </select>
      </div>
      <div class="field">
        <label>公式</label>
        <select class="ui fluid dropdown" name="formula_id">
          @foreach ($formulas as $formula)
            <option value="{{ $formula->id }}">{{ $formula->name }}</option>
          @endforeach
        </select>
      </div>
    </div>
    
    <div class="two fields">
      <div class="field">
        <label>基数</label>
        <input type="number" name="base_count" placeholder="请输入基数" required autofocus>
      </div>
      <div class="field">
        <label>依附于</label>
        <select class="ui fluid dropdown" name="attach">
          <option value="0">最小值</option>
          <option value="1">最大值</option>
        </select>
      </div>
    </div>

    <button class="ui primary button" type="submit">生成</button>
  </form>
@endsection