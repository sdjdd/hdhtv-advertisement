<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <meta charset="UTF-8">
  <title>{{ $title }}</title>
  <link rel="stylesheet" href="{{ asset('asset/semantic-ui-2.4.1/semantic.min.css') }}">
</head>
<body>
<div class="ui container" style="padding: 1rem 0;">
  <h1 class="ui header">{{ $title }}</h1>
  
  @foreach (['success', 'negative'] as $type)
    @if (session($type))
      <div class="ui {{ $type }} message">
        <p>{{ session($type) }}</p>
      </div>
    @endif
  @endforeach

  <table class="ui celled striped table">
    <thead>
      <tr>
        @foreach ($cols as $col)
          <th>{{ $col }}</th>
        @endforeach
        @if (isset($can_del) && $can_del)
          <th>操作</th>
        @endif
      </tr>
    </thead>
    <tbody>
      @foreach ($data as $item)
        <tr>
          @foreach ($item as $key => $val)
            <td>{{ $val ?? '-' }}</td>
          @endforeach
          @if (isset($can_del) && $can_del)
            <td>
              <a href="javascript:delUser('{{ $item->tel }}');">删除</a>
            </td>
          @endif
        </tr>
      @endforeach
    </tbody>
    @if ($data->hasPages())
      <tfoot><tr><th colspan="{{ (isset($can_del) && $can_del) ? count($cols) : count($cols)+1 }}">
        {{ $data->links('_pagination', ['class' => 'mini right floated']) }}
      </th></tr></tfoot>
    @endif
</table>
</div>
@if (isset($can_del) && $can_del)
<form id="del_form" method="POST">
  {{ csrf_field() }}
  {{ method_field('DELETE') }}
  <input id="del_tel" name="tel" value="" type="hidden">
</form>
<script>
  var delForm = document.getElementById("del_form");
  var delTel = document.getElementById("del_tel");
  function delUser(tel) {
    if (confirm("确定要删除手机号为 "+tel+" 的用户吗？")) {
      delTel.value = tel;
      delForm.submit();
    }
  }
</script>
@endif
</body>
</html>