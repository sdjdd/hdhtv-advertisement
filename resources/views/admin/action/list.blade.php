@extends('admin.layout')

@section('title', '活动列表')

@section('content')
  <table class="ui celled table">
    <thead><tr>
      <th>ID</th>
      <th>名称</th>
      <th>开始日期</th>
      <th>结束日期</th>
      <th>操作</th></tr>
    </thead>
    <tbody>
      <tr>
        <td>-</td>
        <td>林肯</td>
        <td>-</td>
        <td>-</td>
        <td><a href="{{ route('admin.action.detail', ['id' => 1]) }}">查看</a></td>
      </tr>
    </tbody>
  </table>
@endsection