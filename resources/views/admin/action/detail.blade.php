<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <meta charset="UTF-8">
  <title>林肯</title>
  <link rel="stylesheet" href="{{ asset('asset/semantic-ui-2.4.1/semantic.min.css') }}">
</head>
<body>
  <div style="padding: 1rem;">
    <h1 class="ui header">林肯注册数据</h1>
    <table class="ui celled striped table">
      <thead><tr>
        <th>姓名</th>
        <th>电话</th>
        <th>城市</th>
        <th>酒店名称</th>
        <th>优惠券代码</th>
        <th>提交时间</th></tr>
      </thead>
      <tbody>
        @foreach ($data as $item)
          <tr>
            <td>{{ $item->name }}</td>
            <td>{{ $item->tel }}</td>
            <td>{{ $item->city }}</td>
            <td>{{ $item->hotel_name }}</td>
            <td>{{ $item->code ?? '-' }}</td>
            <td>{{ $item->created_at }}</td>
          </tr>
        @endforeach
      </tbody>
      @if ($data->hasPages())
        <tfoot><tr><th colspan="6">
          {{ $data->links('_pagination', ['class' => 'mini right floated']) }}
        </th></tr></tfoot>
      @endif
  </table>
  </div>
</body>
</html>