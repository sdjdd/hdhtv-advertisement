@extends('monitor.layout')

@section('title', '广告详情')

@section('content')
  <h1 class="ui header">{{ $advertisement->name }}</h1>
  <table class="ui celled table">
    <thead><tr>
      <th>广告名称</th>
      <th>开始日期</th>
      <th>结束日期</th>
    </tr></thead>
    <tbody><tr>
      <td>{{ $advertisement->name }}</td>
      <td>{{ parseDateString($advertisement->start_date) }}</td>
      <td>{{ parseDateString($advertisement->end_date) }}</td>
    </tr></tbody>
  </table>

  <form>
    <div style="display:inline-block">
      <label style="margin:0 1rem;">广告位:</label>
      <select class="ui dropdown" name="p">
        @foreach($positions as $position)
          <option value="{{ $position->id }}"
            @if($current_position->id === $position->id) selected @endif>
            {{ $position->name }}
          </option>
        @endforeach
      </select>
    </div>
    <div style="display:inline-block;">
      <label style="margin:0 1rem;">日期:</label>
      <select class="ui dropdown" name="date">
        @foreach($date_period as $date)
          <option value="{{ $date->toDateString() }}" @if($date->eq($current_date)) selected @endif>
            {{ $date->format('Y年m月d日') }}
          </option>
        @endforeach
      </select>
    </div>
    <button class="ui primary button" type="submit" style="margin-left:1rem;">
      确定
    </button>
  </form>

  <div class="ui segment">
    @if(count($reports) > 0)
      <table class="ui striped celled table" id="detail">
        <thead><tr>
          <th>日期</th>
          <th>广告位</th>
          <th>酒店</th>
          <th>平台</th>
          <th>曝光量</th>
          <th>点击量</th>
          <th>点击率</th>
        </tr></thead>
        <tbody>
          @foreach($reports as $report)
            <tr>
              <td>{{ parseDateString($report->date) }}</td>
              <td>{{ $current_position->name }}</td>
              <td>{{ $report->hotel_name }}</td>
              <td>{{ $report->platform_name }}</td>
              <td class="pv">{{ $report->pv }}</td>
              <td class="uv">{{ $report->uv }}</td>
              <td class="rate">计算中</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    @else
      暂无数据
    @endif
  </div>
@endsection

@push('scripts')
<script>
  $('#detail tbody tr').each((index, e) => {
    e = $(e)
    let pv = Number($(e.children('.pv')[0]).text())
    let uv = Number($(e.children('.uv')[0]).text())
    let rate = (uv / pv * 100).toFixed(1)
    $(e.children('.rate')[0]).text(rate + ' %')
  })
</script>
@endpush
