<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <meta charset="UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>广告监控 | @yield('title', 'HDHTV')</title>
  <link rel="stylesheet" href="{{ asset('asset/semantic-ui-2.4.1/semantic.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/monitor.css') }}">
</head>
<body>
@include('monitor._topbar')

<div class="banner"></div>

<div class="ui container content">
  @yield('content')
</div>
<script src="{{ asset('asset/jquery-3.3.1/jquery.min.js') }}"></script>
<script src="{{ asset('asset/semantic-ui-2.4.1/semantic.min.js') }}"></script>
<script src="{{ asset('js/monitor.js') }}"></script>
@stack('scripts')
</body>
</html>