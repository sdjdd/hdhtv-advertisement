@extends('monitor.layout')

@section('title', '广告列表')

@section('content')
  <h1 class="ui header">广告列表</h1>

  @if (count($advertisements) > 0)
    <table class="ui table">
      <thead><tr>
        <th class="six wide">广告名称</th>
        <th class="four wide">开始日期</th>
        <th class="four wide">结束日期</th>
        <th class="two wide">操作</th>
      </tr></thead>
      <tbody>
        @foreach ($advertisements as $ad)
          <tr>
            <td>{{ $ad->name }}</td>
            <td>{{ parseDateString($ad->start_date) }}</td>
            <td>{{ parseDateString($ad->end_date) }}</td>
            <td><a href="{{ route('monitor.detail', $ad) }}">查看</a></td>
          </tr>
        @endforeach
      </tbody>
      @if ($advertisements->hasPages())
        <tfoot><tr><th colspan="4">
          {{ $advertisements->links('_pagination', ['class' => 'mini right floated']) }}
        </th></tr></tfoot>
      @endif
    </table>
  @else
    <div class="ui message">
      <div class="header">广告列表为空</div>
      <p>您目前没有广告</p>
    </div>
  @endif
@endsection
