<div class="ui container topbar">
  <a class="logo" href="{{ route('monitor.index') }}"></a>
  <div class="user-menu">
    <span class="item">今天是:
      {{ today()->format('Y年m月d日') }}
    </span>
    <span class="item">欢迎您:</span>
    <div class="ui dropdown">
      <span class="username">{{ Auth::user()->username }}</span>
      <i class="dropdown icon" style="margin:0;"></i>
      <div class="menu user-action">
        <a class="item" href="{{ route('logout') }}">注销登录</a>
        @if (Auth::user()->is_admin)
          <a class="item" href="{{ route('admin.index') }}">后台管理</a>
        @endif
      </div>
    </div>
  </div>
</div>