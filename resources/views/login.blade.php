<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <meta charset="UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="{{ asset('asset/semantic-ui-2.4.1/semantic.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/login.css') }}">
</head>
<body>
<div class="login-container">
  <img class="logo" src="img/logo.png">

  <div class="form-box">
    @if (session('error') || count($errors))
      <div class="ui error message">
        <div class="header">登录失败</div>
        <ul class="list">
          @if (session('error'))
            <li>{{ session('error') }}</li>
          @endif
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif

    <form class="ui form" method="POST">
      {{ csrf_field() }}
      <div class="field">
        <div class="ui left icon input">
          <i class="user icon"></i>
          <input type="text" name="username" value="{{ old('username') }}" placeholder="用户名" autofocus required>
        </div>
      </div>
      <div class="field">
        <div class="ui left icon input">
          <i class="lock icon"></i>
          <input name="password" type="password" placeholder="密码" required>
        </div>
      </div>
      <div class="field" style="margin-left:4px;">
        <div class="ui checkbox">
          <input id="remember" name="remember" type="checkbox" @if(old('remember')) checked @endif>
          <label for="remember">记住登录状态</label>
        </div>
      </div>
      <button class="ui fluid login button" type="submit">登&emsp;录</button>
    </form>
  </div>
  <div class="copyright">© 北京耀莱互动科技有限公司</div>
</div>
</html>