<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DashboardController extends Controller
{
    public function lincolnSanyaUsers(Request $request)
    {
        $title = '林肯三亚进入页面用户数据';
        $cols = [
            '创建时间', '更新时间', '姓名', '电话', '入住酒店'
        ];
        $data = DB::table('act_lincoln_sanya_users')
            ->select([
                'created_at', 'updated_at', 'name', 'tel', 'hotel'
            ])
            ->paginate(50);

        return view('admin.action.lincoln-sanya',
            compact('title', 'cols', 'data'));
    }

    public function lincolnSanyaDriveUsers(Request $request)
    {
        $title = '林肯三亚预约试驾用户数据';
        $cols = [
            '提交时间', '姓名', '电话', '入住酒店', '城市'
        ];
        $data = DB::table('act_lincoln_sanya_drive_users')
            ->select([
                'submit_at', 'name', 'tel', 'hotel', 'city'
            ])
            ->paginate(50);

        return view('admin.action.lincoln-sanya',
            compact('title', 'cols', 'data'));
    }

    public function lincolnSanyaTransUsers(Request $request, $uuid)
    {
        $uuids = [
            '345ce653-48f7-4679-b7f1-721b587679f2' => 'all',
            '1ec33973-d61d-4012-b033-547916d58200' => '中粮三亚美高梅度假酒店',
            '75fee607-a06f-456d-a405-421589845441' => '三亚亚龙湾瑞吉度假酒店',
            'a4abd504-d324-4e3a-8db3-68bd3329df8b' => '金茂三亚希尔顿大酒店',
            'e8c2bfd3-5107-4307-a2a4-0834c26c40de' => '三亚半山半岛洲际',
            '5f8ea391-a5d3-46a6-94ae-5b90e0b7ae3c' => '三亚海棠湾民生威斯汀度假酒店',
            '1d506ed7-eded-43d6-8373-582c16a2ac1b' => '石梅湾艾美酒店',
            '6c4e54f6-033c-4b19-9aeb-848a81e0743f' => '三亚丽思卡尔顿酒店',
            '0f7c1ed5-e35f-4d54-a759-b09635cea234' => '三亚保利瑰丽酒店',
        ];
        if (!array_key_exists($uuid, $uuids)) {
            abort(404);
        }

        $hotel = $uuids[$uuid];
        $title = '林肯三亚预约接驳车用户数据';
        $cols = [
            '提交时间', '姓名', '电话', '入住酒店',
            '日期', '时间段', '目的地', '原所在城市',
        ];
        $can_del = true;
        $query = DB::table('act_lincoln_sanya_trans_users');
        
        if ($hotel !== 'all') {
            $query = $query->where('hotel', $hotel);
            $can_del = false;
        }
        $data = $query
            ->select([
                'submit_at', 'name', 'tel', 'hotel',
                'date', 'period_index', 'dest', 'city'
            ])
            ->paginate(50);

        $periods = Action\LincolnController::$periods;
        for ($i = 0; $i < count($data); $i++) {
            $p_index = intval($data[$i]->period_index);
            $data[$i]->period_index = $periods[$p_index];
        }

        return view('admin.action.lincoln-sanya',
            compact('title', 'cols', 'data', 'can_del'));
    }

    public function lincolnSanyaDelTransUser(Request $request)
    {
        $table_name = 'act_lincoln_sanya_trans_users';
        try {
            $pdo = DB::connection()->getPdo();
            // 为接驳车表加写锁
            $pdo->exec("lock tables {$table_name} write");
            DB::table($table_name)
                ->where('tel', $request->tel)
                ->delete();
        } finally {
            $pdo->exec('unlock tables');
        }
        return back()->with('success', '删除成功！');
    }
}
