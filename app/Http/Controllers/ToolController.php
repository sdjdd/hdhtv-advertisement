<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ToolController extends Controller
{
    const ERR_HOTEL_NOT_EXISTS = [1, '酒店不存在'];
    const ERR_SYNC_NOT_ENABLED = [2, '当前酒店未开启同步数据库功能'];
    const ERR_INVALID_DATA = [3, '数据格式不正确'];
    const ERR_INSERT_FAILED = [4, '插入数据失败'];

    public function __construct() {
        $this->middleware(function (Request $request, $next) {
            if ($request->header('token') === env('TOOL_TOKEN')) {
                return $next($request);
            }
            return response(null, 404);
        });
    }

    public function getLastOperationID($hotel_code) {
        $hotel = DB::table('hotels')->where('code', $hotel_code)->first();
        if (!$hotel) {
            return self::resp_err(self::ERR_HOTEL_NOT_EXISTS);
        }
        if (!$hotel->sync_enabled) {
            return self::resp_err(self::ERR_SYNC_NOT_ENABLED);
        }
        return self::resp($hotel->last_op_id);
    }
    
    public function postOperationData(Request $request, $hotel_code) {
        $hotel = DB::table('hotels')->where('code', $hotel_code)->first();
        if (!$hotel) {
            return self::resp_err(self::ERR_HOTEL_NOT_EXISTS);
        }
        if (!$hotel->sync_enabled) {
            return self::resp_err(self::ERR_SYNC_NOT_ENABLED);
        }
        if ($request->isJson()) {
            $data = $request->all();
            if ($data && is_array($data)) {
                $last_op_id = self::insertOperationData($hotel->id, $data);
                if ($last_op_id === false) {
                    return self::resp_err(self::ERR_INSERT_FAILED);
                }
                DB::table('hotels')
                    ->where('id', $hotel->id)
                    ->update([
                        'last_op_id' => $last_op_id,
                        'last_sync_at' => now(),
                    ]);
                return self::resp();
            }
        }
        return self::resp_err(self::ERR_INVALID_DATA);
    }

    private static function insertOperationData($hotel_id, $data)
    {
        $items = [];
        foreach ($data as $d) {
            $items[] = [
                'operate_id' => $d[0],
                'function_name' => $d[1] ?? '这是一个空值',
                'time' => $d[2],
                'status' => $d[3],
                'stb_ip' => $d[4]
            ];
        }
        try {
            DB::table("operation_data_{$hotel_id}")->insert($items);
            return end($data)[0];;
        } catch (\Exception $e) {
            return false;
        }
    }


    private static function resp_err($error) {
        return self::resp(null, $error[1], $error[0]);
    }
    private static function resp($data = null, $msg = 'ok', $code = 0) {
        return response()->json([
            'code' => $code,
            'msg' => $msg,
            'data' => $data,
        ]);
    }
}
