<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Advertisement;
use App\Models\Position;
use App\Models\Formula;
use App\Models\Access;
use App\Models\User;
use App\Http\Requests\StoreAdvertisement;
use App\Http\Requests\StorePosition;
use App\Http\Requests\FormulaRequest;
use DB;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::check() || !Auth::user()->is_admin) {
                abort(404);
            }
            return $next($request);
        });
    }

    public function index()
    {
        return redirect()->route('admin.ad.list');
    }

    // 广告列表
    public function advertisementList(Request $request)
    {
        $advertisements = Advertisement::with('user');
        if ($request->search) {
            $advertisements->where('name', 'like', "%{$request->search}%");
        }
        $advertisements = $advertisements
            ->paginate(20)
            ->appends($request->except('page'));
        return view('admin.advertisement.list', compact('advertisements'));
    }

    // 创建广告
    public function advertisementCreate()
    {
        return view('admin.advertisement.create_edit');
    }

    // 保存广告
    public function advertisementStore(StoreAdvertisement $request)
    {
        $data = $request->all();
        $data['access_enabled'] = $request->has('access_enabled');
        $ad = Advertisement::create($data);
        return redirect()->route('admin.ad.edit', $ad)
            ->with('success', '创建成功！');
    }

    // 编辑广告
    public function advertisementEdit(Advertisement $advertisement)
    {
        return view('admin.advertisement.create_edit', compact('advertisement'));
    }

    // 更新广告
    public function advertisementUpdate(Advertisement $advertisement, StoreAdvertisement $request)
    {
        $data = $request->all();
        $data['access_enabled'] = $request->has('access_enabled');
        $advertisement->update($data);
        return redirect()->route('admin.ad.edit', $advertisement)
            ->with('success', '更新成功！');
    }

    // 删除广告
    public function advertisementDelete(Advertisement $advertisement)
    {
        $advertisement->delete();
        return redirect()->route('admin.ad.list')
            ->with('success', "广告 {$advertisement->name} 删除成功！");
    }

    // 创建广告位
    public function positionCreate(Request $request)
    {
        if (! $advertisement = Advertisement::find($request->ad_id)) {
            return back()->with('error', '当前广告已被删除');
        }
        return view('admin.position.create_edit', compact('advertisement'));
    }

    // 保存广告位
    public function positionStore(StorePosition $request)
    {
        Position::create($request->all());
        return redirect()
            ->route('admin.ad.edit', $request->ad_id)
            ->with('success', "广告位 {$request->name} 创建成功！");
    }

    // 更新广告位
    public function positionUpdate(StorePosition $request, Position $position)
    {
        $position->update($request->all());
        return redirect()->route('admin.position.edit', $position)->with('success', '更新成功！');
    }

    // 编辑广告位
    public function positionEdit(Request $request, Position $position)
    {
        $accesses = $position->accesses()
            ->selectRaw('date(time) as date, sum(expected_count) as expected_count, sum(count) as count')
            ->groupBy('date')
            ->orderBy('date')
            ->get();
        return view('admin.position.create_edit', compact('position', 'accesses'));
    }

    // 公式列表
    public function formulaList()
    {
        $formulas = Formula::paginate(20);
        return view('admin.formula.list', compact('formulas'));
    }

    // 创建公式
    public function formulaCreate()
    {
        return view('admin.formula.create_edit');
    }

    // 保存公式
    public function formulaStore(FormulaRequest $request, Formula $formula)
    {
        $content = [];
        for ($i = 0; $i < 24; $i++) {
            $content[$i][0] = intval($request->min[$i]);
            $content[$i][1] = intval($request->max[$i]);
        }
        $formula->name = $request->name;
        $formula->remark = $request->remark;
        $formula->content = json_encode($content);
        $formula->save();
        return redirect()->route('admin.formula.edit', $formula)
            ->with('success', isset($formula->id) ? '更新成功！' : '创建成功！');
    }

    // 编辑公式
    public function formulaEdit(Formula $formula)
    {
        $formula->content = json_decode($formula->content, true);
        return view('admin.formula.create_edit', compact('formula'));
    }

    // 删除公式
    public function formulaDelete(Formula $formula)
    {
        $formula->delete();
        return redirect()->route('admin.formula.list')
            ->with('success', "公式 {$formula->name} 删除成功！");
    }

    //创建访问记录
    public function accessCreate(Request $request)
    {
        $position = Position::find($request->position_id);
        if ($position === null) {
            return back()->with('error', '当前广告位已被删除');
        }

        $dates = $position->getFreeAccessDates();
        $formulas = Formula::select('id', 'name')->get();

        return view('admin.access.create', compact('dates', 'formulas', 'position'));
    }

    // 保存访问记录
    public function accessStore(Request $request)
    {
        $position = Position::find($request->position_id);
        if ($position === null) {
            return back()->with('error', "ID为 {$request->position_id} 的广告位不存在");
        }
        
        $result = $position->setAccesses(
            $request->date,
            $request->formula_id,
            $request->base_count,
            $request->attach == 1
        );
        if ($result !== null) {
            return back()->with('error', $result);
        }
        return redirect()->route('admin.access.edit', [
            'position_id' => $position->id,
            'date' => $request->date,
        ])->with('success', '访问记录生成成功!');
    }

    // 编辑访问记录
    public function accessEdit(Request $request)
    {
        $position = Position::find($request->position_id);
        if(! $position) {
            return '';//return back()->with('error', '当前广告位已被删除');
        }
        $accesses = $position->accesses()->select('id', 'expected_count')
            ->whereRaw('date(time) = ?', $request->date)
            ->get();
        return view('admin.access.edit', compact('position', 'accesses'));
    }

    // 更新访问记录
    public function accessUpdate(Request $request)
    {
        $position = Position::find($request->position_id);
        if(! $position) {
            return back()->with('error', '当前广告位已被删除');
        }
        $ids = $request->id;
        $olds = $request->old;
        $datas = $request->data;
        $change = 0;
        for ($i = 0; $i < count($datas); $i++) {
            if ($olds[$i] !== $datas[$i]) {
                Access::where('id', $ids[$i])->update(['expected_count' => $datas[$i]]);
                $change++;
            }
        }
        if ($change === 0) {
            session()->flash('info', '没有需要修改的值');
        } else {
            session()->flash('success', $change.' 个值修改成功!');
        }
        return redirect()->route('admin.access.edit', [
            'position_id' => $position->id,
            'date' => $request->date,
        ]);
    }

    // 用户列表
    public function userList(Request $request)
    {
        $users = User::select('id', 'username', 'remark', 'created_at');
        if ($request->search) {
            $users->where('username', 'like', "%{$request->search}%");
        }
        $users = $users->where('is_admin', false)->paginate(20);
        return view('admin.user.list', compact('users'));
    }

    // 创建用户
    public function userCreate()
    {
        return view('admin.user.create');
    }

    // 保存用户
    public function userStore(Request $request, User $user)
    {
        $this->validate($request, [
            'username' => 'required|unique:users|min:4|max:20',
            'password' => 'required|min:6|max:255|confirmed',
            'password_confirmation' => 'required',
            'remark' => 'nullable|max:255',
        ]);
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->remark = $request->remark;
        $user->save();

        return redirect()
            ->route('admin.user.detail', $user)
            ->with('success', '创建成功！');
    }

    // 用户详情
    public function userDetail(User $user)
    {
        if ($user->is_admin) {
            abort(403);
        }
        return view('admin.user.detail', compact('user'));
    }

    // 活动列表
    public function actionList()
    {
        return view('admin.action.list');
    }

    // 活动详情
    public function actionDetail($id)
    {
        $data = DB::table('act_lincoln_users as a')
            ->leftJoin('act_lincoln_coupons as b', 'a.id', '=', 'b.user_id')
            ->orderBy('a.id')
            ->paginate(20);
        return view('admin.action.detail', compact('data'));
    }
}
