<?php

namespace App\Http\Controllers\Action;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;

class LincolnController extends Controller
{
    public static $periods = [
        '09:00-11:00',
        '11:00-13:00',
        '14:00-16:00',
        '16:00-17:00',
    ];

    public function store(Request $request)
    {
        $response = [
            'success' => false,
            'message' => '',
        ];
        $now = now();
        if ($now < Carbon::create(2019, 4, 10)) {
            $response['message'] = '活动还未开始，敬请期待！';
        } else if ($now >= Carbon::create(2019, 4, 25)) {
            $response['message'] = '活动已经结束，感谢您的关注！';
        } else {
            $response['success'] = true;
            $user = DB::table('act_lincoln_users as a')
                ->leftJoin('act_lincoln_coupons as b', 'a.id', '=', 'b.user_id')
                ->select('a.created_at', 'b.code')
                ->where('a.tel', $request->tel)
                ->first();
            if (!$user) {
                $data = [
                    'name' => $request->name,
                    'tel'  => $request->tel,
                    'city' => $request->city,
                    'hotel_code' => $request->hotelCode,
                    'hotel_name' => $request->hotelName,
                    'created_at' => $now
                ];
                try {
                    $userId = DB::table('act_lincoln_users')->insertGetId($data);
                    $response['message'] = '提交成功，感谢您的参与。';
                    if ($couponCode = self::requestCoupon($request->hotelCode, $userId)) {
                        $response['coupon_code'] = $couponCode;
                    }
                } catch (\Exception $e) {
                    $response['success'] = false;
                    $response['message'] = '提交信息有误！';
                }
            } else {
                $response['message'] = "您已于 {$user->created_at} 成功提交信息。";
                if ($user->code) {
                    $response['coupon_code'] = $user->code;
                }
            }
        }

        return $response;
    }

    private static function requestCoupon($hotelCode, $userId)
    {
        $couponCode = null;
        $pdo = DB::connection()->getPdo();
        try {
            $pdo->exec('lock table act_lincoln_coupons write');
            $coupon = DB::table('act_lincoln_coupons')
                ->where('hotel_code', $hotelCode)
                ->whereNull('user_id')
                ->first();
            // 优惠券有剩余
            if ($coupon) {
                $couponCode = $coupon->code;
                DB::table('act_lincoln_coupons')
                    ->where('id', $coupon->id)
                    ->update(['user_id' => $userId]);
            }
        } finally {
            $pdo->exec('unlock tables');
        }
        return $couponCode;
    }

    public function getAvailableTime(Request $request)
    {
        if (!$request->hotel || !$request->date) {
            return self::createRespErr('请选择入住酒店和预约日期');
        }

        $periods = [];
        foreach (self::$periods as $p) {
            $periods[$p] = true;
        }
        if ($request->hotel === '石梅湾艾美酒店') {
            $periods[self::$periods[1]] = false;
            $periods[self::$periods[3]] = false;
        }

        return self::createRespOK(compact('periods'));
    }

    public function postUserData(Request $request) {
        $data = [
            'tel' => $request->tel,
            'name' => $request->name,
            'hotel' => $request->hotel,
        ];
        foreach ($data as $key => $val) {
            if ($val === null) {
                return self::createRespErr("请输入${key}");
            }
        }

        $table = DB::table('act_lincoln_sanya_users');
        $user = $table->where('tel', $data['tel'])->first();
        if ($user) {
            $data['updated_at'] = now();
            $table->where('id', $user->id)->update($data);
        } else {
            $data['created_at'] = now();
            $table->insert($data);
        }

        return self::createRespOK();
    }

    public function postDriveData(Request $request)
    {
        $data = [
            'tel' => $request->tel,
            'name' => $request->name,
            'hotel' => $request->hotel,
            'city' => $request->city,
        ];
        foreach ($data as $key => $val) {
            if ($val === null) {
                return self::createRespErr("请输入${key}");
            }
        }
        $data['submit_at'] = now();

        $table = DB::table('act_lincoln_sanya_drive_users');
        $user = $table->where('tel', $data['tel'])->first();
        if ($user) {
            return self::createRespMsg("您已预约试驾，请勿重复提交。");
        }

        $table->insert($data);
        return self::createRespMsg('提交成功，感谢您的参与！请耐心等待，稍后会有工作人员与您联系。');
    }

    public function postTransData(Request $request)
    {
        $data = [
            'tel' => $request->tel,
            'name' => $request->name,
            'hotel' => $request->hotel,
            'date' => $request->date,
            'period_index' => $request->period,
            'dest' => $request->dest,
            'city' => $request->city,
        ];
        foreach ($data as $key => $val) {
            if ($val === null) {
                return self::createRespErr("请输入${key}");
            }
        }
        $data['submit_at'] = now();

        $table = DB::table('act_lincoln_sanya_trans_users');
        $user = $table->where('tel', $data['tel'])->first();
        if ($user) {
            return self::createRespMsg("您已预约接驳车，请勿重复提交。");
        }

        $table->insert($data);

        return self::createRespMsg('提交成功，感谢您的参与！请耐心等待，稍后会有工作人员与您联系。');
    }

    private static function createResponse($success, $message, $data)
    {
        $resp = compact('success', 'message');
        if ($data !== null) {
            foreach ($data as $key => $val) {
                $resp[$key] = $val;
            }
        }
        return $resp;
    }

    private static function createRespOK($data = null)
    {
        return self::createResponse(true, 'ok', $data);
    }

    private static function createRespErr($message)
    {
        return self::createResponse(false, $message, null);
    }

    private static function createRespMsg($message)
    {
        return self::createResponse(true, $message, null);
    }
}
