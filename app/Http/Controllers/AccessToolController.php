<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;

class AccessToolController extends Controller
{
    public function getTasks()
    {
        $today = today();
        $tasks = DB::table('advertisements as a')
            ->join('advertisement_positions as b', 'a.id', '=', 'b.ad_id')
            ->join('advertisement_accesses as c', 'b.id', '=', 'c.position_id')
            ->select('c.id', 'c.position_id', 'b.url', 'c.expected_count', 'c.count')
            ->where('a.access_enabled', true)
            ->where('a.start_date', '<=', $today)
            ->where('a.end_date', '>=', $today)
            ->where('c.time', now()->minute(0)->second(0))
            ->get();
        return $tasks;
    }

    public function putCount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'    => 'required|exists:advertisement_accesses',
            'count' => 'required|integer|min:1'
        ]);
        $response = [
            'success' => true,
            'message' => 'ok'
        ];
        if ($validator->fails()) {
            $response['success'] = false;
            $response['message'] = $validator->errors()->first();
        } else {
            DB::table('advertisement_accesses')
                ->where('id', $request->id)
                ->increment('count', $request->count);
        }
        return $response;
    }
}
