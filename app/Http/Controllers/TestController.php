<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller {

    public function canyinhuli() {
        return response()->json([
            'code' => 0,
            'message' => '成功',
            'data' => [
                'id' => '123456',
                'name' => '任我行',
                'sex' => '男',
                'age' => 60,
                'communityName' => '北京燕园',
                'roomName' => '3#1-329',
                'permission' => 1 
            ]
        ]);
    }
}

