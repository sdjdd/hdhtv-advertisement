<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Share;

class StaticPageController extends Controller
{
    public function home()
    {
        return redirect()->route('monitor.index');
    }

    public function login()
    {
        if (Auth::check()) {
            return redirect()->route('home');
        } else {
            return view('login');
        }
    }

    public function shareLink($hash)
    {
        $share = Share::select('url')
            ->where('hash', $hash)
            ->first();
        if ($share) {
            return redirect($share->url);
        }
        abort(404);
    }
}
