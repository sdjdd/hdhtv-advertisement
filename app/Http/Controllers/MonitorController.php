<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use App\Models\Advertisement;
use App\Models\Position;
use Auth;

class MonitorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        if ($user->is_admin) {
            $advertisements = new Advertisement;
        } else {
            $advertisements = $user->advertisements();
        }
        $advertisements = $advertisements->select('id', 'name', 'start_date', 'end_date')->paginate(15);

        return view('monitor.index', compact('advertisements'));
    }

    public function detail(Request $request, Advertisement $advertisement)
    {
        $this->authorize('view', $advertisement);

        $start_date = Carbon::parse($advertisement->start_date);
        $end_date = Carbon::parse($advertisement->end_date);

        $validator = Validator::make($request->all(), [
            'p' => 'numeric',
            'date' => "date|after_or_equal:${start_date}|before_or_equal:${end_date}",
        ]);
        if ($validator->fails()) {
            return back();
        }

        $positions = $advertisement->positions()->select('id', 'name')->get();
        if ($request->has('p')) {
            $position = $positions->where('id', $request->p)->first();
            if (!$position) {
                abort(404);
            }
        } else {
            $position = $positions->first();
        }
        
        $date_period = CarbonPeriod::create($start_date, $end_date);
        if ($request->has('date')) {
            $current_date = Carbon::parse($request->date);
        } else {
            $yesterday = today()->subDay();
            if ($yesterday->gt($start_date)) {
                $current_date = $yesterday;
                if ($end_date->lt($current_date)) {
                    $current_date = $end_date;
                }
            } else {
                $current_date = $start_date;
            }
        }

        $reports = [];
        if ($position) {
            $reports = $position->reports()
            ->where('date', $current_date)
            ->where('published', true)
            ->get();
        }

        return view('monitor.detail', [
            'advertisement' => $advertisement,
            'date_period' => $date_period,
            'current_date' => $current_date,
            'positions' => $positions,
            'current_position' => $position,
            'reports' => $reports,
        ]);
    }

}
