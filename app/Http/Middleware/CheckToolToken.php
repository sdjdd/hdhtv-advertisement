<?php

namespace App\Http\Middleware;

use Closure;

class CheckToolToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = env('TOOL_TOKEN');
        if ($token === null || $request->header('token') !== $token) {
            abort(404);
        }
        return $next($request);
    }
}
