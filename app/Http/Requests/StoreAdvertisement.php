<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdvertisement extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'user_id' => 'nullable|numeric|exists:users,id',
            'action_id' => 'nullable|numeric',
            'start_date' => 'required|date',
            'end_date' => 'required|after_or_equal:start_date',
            'remark' => 'nullable|max:255',
        ];
    }

    public function messages()
    {
        return [
            'end_date.after_or_equal' => '结束日期必须大于或等于开始日期',
        ];
    }

    public function attributes()
    {
        return [
            'name' => '广告名称',
            'user_id' => '所属用户',
        ];
    }
}
