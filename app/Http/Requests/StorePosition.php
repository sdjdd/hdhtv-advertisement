<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePosition extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ad_id' => 'nullable|numeric|exists:advertisements,id',
            'name' => 'required|max:255',
            'url' => 'nullable|max:255',
            'remark' => 'nullable|max:255',
        ];
    }

    public function attributes()
    {
        return [
            'ad_id' => '所属广告',
        ];
    }
}
