<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormulaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'remark' => 'nullable',
            'min' => 'required|size:24',
            'max' => 'required|size:24',
            'min.*' => 'required|integer|min:0',
            'max.*' => 'required|integer|min:0',
        ];
    }
}
