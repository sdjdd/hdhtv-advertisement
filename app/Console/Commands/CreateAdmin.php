<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '创建管理员账户';

    protected $user;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        parent::__construct();

        $this->user = $user;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = $this->user;
        $user->username = $this->ask('用户名');
        $user->password = bcrypt($this->secret('密码'));
        $user->is_admin = true;
        if (User::where('username', $user->username)->first()) {
            $this->error('用户名已存在');
        } else {
            $user->save();
            $this->info('用户 ' . $user->username . ' 创建成功');
        }
    }
}
