<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

// 广告
class Advertisement extends Model
{
    protected $fillable = [
        'user_id', 'action_id', 'name', 'start_date', 'end_date', 'access_enabled', 'remark',
    ];

    public function positions()
    {
        return $this->hasMany(Position::class, 'ad_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
}
