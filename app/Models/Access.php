<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

// 广告位访问
class Access extends Model
{
    protected $table = 'advertisement_accesses';
    public $timestamps = false;
    
    public function position()
    {
        return $this->belongsTo(Position::class, 'position_id', 'id');
    }
    
}
