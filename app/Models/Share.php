<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
    private static $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    public static function toShortURL($url)
    {
        $salt = 'hdhtv';
        $hash = md5($salt.$url);
        $len = strlen($hash) / 4;

        $short_urls = [];
        for ($i = 0; $i < 4; $i++) {
            $piece = substr($hash, $i*$len, $len);
            $hex = hexdec($piece) & 0x3fffffff;
            $short_url = '';
            for ($j = 0; $j < 6; $j++) {
                $short_url .= self::$charset[$hex & 0x0000003d];
                $hex = $hex >> 5;
            }
        
            $short_urls[] = $short_url;
        }
        return $short_urls;
    }

    // 覆盖基类 save
    public function save(array $options = [])
    {
        if ($this->hash === null) {
            $url = $this->url;
            while (true) {
                $hashs = self::toShortURL($url);
                foreach ($hashs as $hash) {
                    if ($this->where('hash', $hash)->count() === 0) {
                        $this->hash = $hash;
                        $success = true;
                        break 2;
                    }
                }
                $url = hash('sha256', $url);
            }
        }
        parent::save($options);
    }
}
