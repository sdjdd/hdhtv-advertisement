<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    
    protected $fillable = ['password', 'remark'];
    protected $hidden = ['password', 'remember_token'];

    public function advertisements()
    {
        return $this->hasMany('App\Models\Advertisement', 'user_id', 'id');
    }

}
