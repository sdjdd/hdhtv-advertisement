<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'advertisement_reports';
    public $timestamps = false;
    
}
