<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\CarbonPeriod;

// 广告位
class Position extends Model
{
    protected $table = 'advertisement_positions';

    protected $fillable = [
        'ad_id', 'name', 'url', 'remark',
    ];


    public function advertisement()
    {
        return $this->belongsTo(Advertisement::class, 'ad_id', 'id');
    }
    
    public function reports()
    {
        return $this->hasMany(Report::class, 'position_id', 'id');
    }

    public function accesses()
    {
        return $this->hasMany(Access::class, 'position_id', 'id');
    }

    // 获取还未设置访问记录的日期
    public function getFreeAccessDates()
    {
        $ad = $this->advertisement;
        $dates = CarbonPeriod::create($ad->start_date, $ad->end_date)->toArray();
        foreach ($dates as &$date) {
            $date = $date->toDateString();
        }

        $accesses = $this->accesses()
            ->selectRaw('distinct date(time) as date')
            ->get();

        return array_diff($dates, $accesses->pluck('date')->toArray());
    }

    // 根据基数和公式设置指定日期的访问记录
    public function setAccesses($date, $formulaId, $baseCount, $attachMax = false)
    {
        if (! $formula = Formula::find($formulaId)) {
            return '公式不存在';
        }
        $formula = json_decode($formula->content, true);

        $sum = 0;
        $randomNum = [];
        $index = $attachMax ? 1 : 0;
        foreach ($formula as $pair) {
            $sum += $pair[$index];
            $randomNum[] = mt_rand($pair[0], $pair[1]);
        }
        $baseCount /= $sum;

        $data = [];
        for ($i = 0; $i < count($randomNum); $i++) {
            $hour = $i < 10 ? "0{$i}" : $i;
            $num = round($randomNum[$i] * $baseCount);
            $data[] = [
                'position_id' => $this->id,
                'time' => "{$date} {$hour}:00:00",
                'expected_count' => $num,
            ];
        }

        Access::insert($data);
    }

}
