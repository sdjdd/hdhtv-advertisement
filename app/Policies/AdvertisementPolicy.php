<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Advertisement;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdvertisementPolicy
{
    use HandlesAuthorization;

    public function before(User $user)
    {
        if ($user->is_admin) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the advertisement.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Advertisement  $advertisement
     * @return mixed
     */
    public function view(User $user, Advertisement $advertisement)
    {
        return $user->id === $advertisement->user_id;
    }

    /**
     * Determine whether the user can create advertisements.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the advertisement.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Advertisement  $advertisement
     * @return mixed
     */
    public function update(User $user, Advertisement $advertisement)
    {
        //
    }

    /**
     * Determine whether the user can delete the advertisement.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Advertisement  $advertisement
     * @return mixed
     */
    public function delete(User $user, Advertisement $advertisement)
    {
        //
    }
}
